# Meter-reader
## Description
This application allows you to track your house electricity consumption

There are 2 endpoint which you can use.

### Endpoints

* Retrieve electricity consumptions based on the request: `POST /api/consumption/read`
* Retrieve the details of a single topic: `POST /api/meter/add`

#### Requirement
Java 11

#### Build the application
In the project root directory:

`mvn clean install`

#### Run the application
In the project web module:

`mvn spring-boot:run`

### Initial data
while the application has starting, initial data will be persisted to the
embedded h2 database.

The initial data which you can use, while you are using the application are the following:

### Client ids: 1,2,3

### Addresses:

    client 1 --> {id: 1, country: Hungary, city: Debrecen, street: Piac, houseNumber: 21}
    client 1 --> {id: 2, country: Hungary, city: Debrecen, street: Vendeg, houseNumber: 2}
    client 2 --> {id: 3, country: Hungary, city: Debrecen, street: Kossuth, houseNumber: 7}
    client 3 --> {id: 4, country: Hungary, city: Debrecen, street: Blahane, houseNumber: 3}

### Consumptions:

    addressId 1 --> [
        {measuredValue : 21, year: 2019, month: MARCH },
        {measuredValue : 51, year: 2019, month: APRIL },
        {measuredValue : 10, year: 2019, month: FEBRUARY },
        {measuredValue : 32, year: 2019, month: JANUARY },
    ]    
    addressId 2 --> [
        {measuredValue : 65, year: 2019, month: OCTOBER },
        {measuredValue : 30, year: 2019, month: APRIL },
        {measuredValue : 23, year: 2019, month: FEBRUARY },
        {measuredValue : 25, year: 2019, month: JANUARY },
    ]  
    addressId 3 --> [
        {measuredValue : 26, year: 2019, month: MARCH },
        {measuredValue : 23, year: 2019, month: APRIL },
        {measuredValue : 12, year: 2019, month: FEBRUARY },
        {measuredValue : 18, year: 2019, month: JANUARY },
        ]  
     addressId 4 --> [
        {measuredValue : 52, year: 2019, month: MARCH },
        {measuredValue : 10, year: 2019, month: APRIL },
        {measuredValue : 9, year: 2019, month: FEBRUARY },
        {measuredValue : 90, year: 2019, month: JANUARY },
        ] 
        
### Meters:
    Meters also initialized, however it is not necessary to create
    request.
    
#### Testing
You can test easily all functionality on the following link:

#### Swagger-ui url: http://localhost:8080/swagger-ui.html#

month and operator field must be UPPERCASE.(e.g.: FEBRUARY, JANUARY, SUM, AVG )

Important!

#### To get aggregated measured value, you have to fill the following fields:

`operator`

Available aggregation operators: SUM, AVG, MIN, MAX

`clientId`

It can be 1, 2 or 3

`address`

Look above the initialized addresses.

`year`

2019

#### To get monthly electricity consumption, you have to fill the following fields:

`operator`

set to null

`month`

You can find the existing measured values at the initial data section.

`year`

2019

`clientId`

It can be 1, 2 or 3

`address`

Look above the initialized addresses.

#### To get yearly electricity consumption, you have to fill the following fields:

`operator`

set to null

`month`

set to null

`clientId`

It can be 1, 2 or 3

`address`

Look above the initialized addresses.

`year`

2019

#### To add an electricity consumption to an existing meter:

`clientId`

It can be 1, 2 or 3

`address`

Look above the initialized addresses.

`consumptionHistory`

you have to fill the year, month and measuredValue fields.
