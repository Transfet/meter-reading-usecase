package com.theitsolutions.meter.reader.web.controller;

import static org.springframework.util.Assert.notNull;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.theitsolutions.meter.reader.service.consumption.ElectricityConsumptionService;
import com.theitsolutions.meter.reader.service.consumption.domain.request.ConsumptionHistoryRequest;
import com.theitsolutions.meter.reader.service.consumption.domain.response.ElectricityConsumptionInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Rest controller for electricity consumption.
 */
@RestController
@Api("MeterEntity reading")
public class ElectricityConsumptionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ElectricityConsumptionController.class);
    private static final String READ_PATH = "/api/consumption/read";

    private ElectricityConsumptionService electricityConsumptionService;

    @Autowired
    public ElectricityConsumptionController(ElectricityConsumptionService electricityConsumptionService) {
        this.electricityConsumptionService = electricityConsumptionService;
    }

    /**
     * Endpoint for reading electricity consumption history by {@link ConsumptionHistoryRequest}.
     * @param consumptionHistoryRequest - given request object.
     * @return - An {@link ElectricityConsumptionInfo} wrapped by a ResponseEntity.
     */
    @PostMapping(READ_PATH)
    @ApiOperation(value = "reading", notes = "Read the electricity consumption history", nickname = "read")
    public ResponseEntity<ElectricityConsumptionInfo> readConsumptionHistory(@Valid @RequestBody ConsumptionHistoryRequest consumptionHistoryRequest) {
        LOGGER.info("Reading consumption history has started");
        notNull(consumptionHistoryRequest, "Electricity consumption request must not be null");
        ElectricityConsumptionInfo consumptionInfo = electricityConsumptionService.readElectricityConsumptionHistory(consumptionHistoryRequest);

        return ResponseEntity.ok(consumptionInfo);
    }
}
