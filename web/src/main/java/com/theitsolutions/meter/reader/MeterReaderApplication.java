package com.theitsolutions.meter.reader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Class for initializing the application.
 */
@SpringBootApplication
@EnableJpaRepositories
public class MeterReaderApplication {

    public static void main(String... args) {
        SpringApplication.run(MeterReaderApplication.class, args);
    }
}
