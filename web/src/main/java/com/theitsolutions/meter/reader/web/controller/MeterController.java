package com.theitsolutions.meter.reader.web.controller;

import static org.springframework.util.Assert.notNull;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.theitsolutions.meter.reader.persistence.dao.exception.ConsumptionHistoryAlreadyExistsException;
import com.theitsolutions.meter.reader.service.meter.MeterService;
import com.theitsolutions.meter.reader.service.meter.domain.request.MeterUpdateRequest;
import com.theitsolutions.meter.reader.service.meter.domain.response.MeterUpdateInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Rest controller for meter.
 */
@RestController
@Api("MeterEntity updating")
public class MeterController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MeterController.class);

    private static final String ADD_CONSUMPTION_PATH = "/api/meter/add";

    private MeterService meterService;

    @Autowired
    public MeterController(MeterService meterService) {
        this.meterService = meterService;
    }

    /**
     * Endpoint for adding consumption history to an existing meter.
     * @param meterUpdateRequest - given {@link MeterUpdateRequest}.
     * @return A response entity.
     */
    @PostMapping(ADD_CONSUMPTION_PATH)
    @ApiOperation(value = "add", notes = "Add an electricity consumption to a specific meter", response = MeterUpdateInfo.class, nickname = "read")
    public ResponseEntity addConsumptionHistory(@Valid @RequestBody MeterUpdateRequest meterUpdateRequest) {
        LOGGER.info("Meter updating has started");

        notNull(meterUpdateRequest, "Electricity consumption request must not be null");
        ResponseEntity response;

        MeterUpdateInfo.MeterUpdateInfoBuilder updateInfoBuilder = MeterUpdateInfo.builder()
            .electricityConsumption(meterUpdateRequest.getElectricityConsumption())
            .updateInformation("Electricity consumption added successfully");

        try {

            meterService.addConsumptionHistory(meterUpdateRequest);
            response = ResponseEntity.ok(updateInfoBuilder.build());

        } catch (ConsumptionHistoryAlreadyExistsException e) {
            response = ResponseEntity.badRequest().body("Electricity consumption has already exists");
        }

        return response;
    }
}
