package com.theitsolutions.meter.reader.service.consumption.operator.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.List;

/**
 * Unit test for {@link SumConsumptionAggregationOperator}.
 */
public class SumConsumptionAggregationOperatorTest {

    private SumConsumptionAggregationOperator underTest;

    @BeforeMethod
    public void setUp() {
        underTest = new SumConsumptionAggregationOperator();
    }

    @Test(dataProvider = "measuredValues")
    public void testCalculateAggregatedValueShouldReturnAverageValueOfList(List<BigDecimal> values, BigDecimal expectedResult) {

        // given

        // when
        BigDecimal result = underTest.calculateAggregatedValue(values);

        // then
        assertThat(result, is(expectedResult));

    }

    @DataProvider
    private Object[][] measuredValues(){
        return new Object[][]{
                {List.of(BigDecimal.valueOf(10), BigDecimal.valueOf(20)), BigDecimal.valueOf(30)},
        };
    }
}
