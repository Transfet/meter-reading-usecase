package com.theitsolutions.meter.reader.service.mapper;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.service.domain.ElectricityConsumption;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.Month;
import java.util.List;

/**
 * Unit test for {@link ElectricityConsumptionMapper}.
 */
public class ElectricityConsumptionMapperTest {

    private ElectricityConsumptionMapper underTest;

    @BeforeMethod
    public void setUp() {
        underTest = new ElectricityConsumptionMapper();
    }

    @Test
    public void testMapToElectricityConsumptionHistoryShouldReturnConsumptionEntity() {

        // given
        ElectricityConsumption electricityConsumption = ElectricityConsumption.builder()
                .measuredValue(BigDecimal.ONE)
                .month(Month.APRIL)
                .year(2019)
                .build();

        // when
        ElectricityConsumptionEntity consumptionHistory = underTest.mapToElectricityConsumptionHistory(electricityConsumption);


        // then
        assertThat(consumptionHistory.getYear(), is(electricityConsumption.getYear()));
        assertThat(consumptionHistory.getMonth(), is(electricityConsumption.getMonth()));
        assertThat(consumptionHistory.getMeasuredValue(), is(electricityConsumption.getMeasuredValue()));

    }

    @Test
    public void testMapToElectricityConsumptionShouldReturnConsumption() {

        // given
        ElectricityConsumptionEntity electricityConsumptionEntity = ElectricityConsumptionEntity.builder()
                .measuredValue(BigDecimal.ONE)
                .month(Month.APRIL)
                .year(2019)
                .build();

        // when
        ElectricityConsumption consumption = underTest.mapToElectricityConsumption(electricityConsumptionEntity);


        // then
        assertThat(consumption.getYear(), is(electricityConsumptionEntity.getYear()));
        assertThat(consumption.getMonth(), is(electricityConsumptionEntity.getMonth()));
        assertThat(consumption.getMeasuredValue(), is(electricityConsumptionEntity.getMeasuredValue()));

    }

    @Test
    public void testMapToElectricityConsumptionsShouldReturnListOfConsumption() {

        // given
        ElectricityConsumptionEntity electricityConsumptionEntity = ElectricityConsumptionEntity.builder()
                .measuredValue(BigDecimal.ONE)
                .month(Month.APRIL)
                .year(2019)
                .build();

        List<ElectricityConsumptionEntity> consumptionHistories = List.of(electricityConsumptionEntity);

        // when
        List<ElectricityConsumption> electricityConsumptions = underTest.mapToElectricityConsumptions(consumptionHistories);


        // then
        assertThat(electricityConsumptions, Matchers.hasSize(1));
        ElectricityConsumption consumption = electricityConsumptions.get(0);

        assertThat(consumption.getYear(), is(electricityConsumptionEntity.getYear()));
        assertThat(consumption.getMonth(), is(electricityConsumptionEntity.getMonth()));
        assertThat(consumption.getMeasuredValue(), is(electricityConsumptionEntity.getMeasuredValue()));

    }


}
