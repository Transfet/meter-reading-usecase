package com.theitsolutions.meter.reader.service.mapper;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import com.theitsolutions.meter.reader.persistence.entity.AddressEntity;
import com.theitsolutions.meter.reader.service.domain.Address;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit test for {@link AddressMapper}.
 */
public class AddressEntityMapperTest {

    private AddressMapper underTest;

    @BeforeMethod
    public void setUp() {
        underTest = new AddressMapper();
    }

    @Test
    public void testMapToAddressEntityShouldReturnAddressEntity() {

        // given
       Address address = com.theitsolutions.meter.reader.service.domain.Address.builder()
                .city("TEST_CITY")
                .houseNumber(21)
                .country("TEST_COUNTY")
                .street("TEST_STREET")
                .build();

        // when
        AddressEntity addressEntity = underTest.mapToAddressEntity(address);

        // then
        assertThat(addressEntity.getCity(), is(address.getCity()));
        assertThat(addressEntity.getCountry(), is(address.getCountry()));
        assertThat(addressEntity.getHouseNumber(), is(address.getHouseNumber()));
        assertThat(addressEntity.getStreet(), is(address.getStreet()));

    }
}
