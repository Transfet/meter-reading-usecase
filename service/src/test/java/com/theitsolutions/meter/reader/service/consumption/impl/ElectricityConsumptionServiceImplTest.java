package com.theitsolutions.meter.reader.service.consumption.impl;

import com.theitsolutions.meter.reader.persistence.dao.AddressDao;
import com.theitsolutions.meter.reader.persistence.dao.ElectricityConsumptionHistoryDao;
import com.theitsolutions.meter.reader.persistence.dao.MeterDao;
import com.theitsolutions.meter.reader.persistence.entity.AddressEntity;
import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.persistence.entity.MeterEntity;
import com.theitsolutions.meter.reader.service.consumption.domain.request.ConsumptionHistoryRequest;
import com.theitsolutions.meter.reader.service.consumption.domain.response.AggregatedConsumptionInfo;
import com.theitsolutions.meter.reader.service.consumption.domain.response.ElectricityConsumptionInfo;
import com.theitsolutions.meter.reader.service.consumption.domain.response.MonthlyConsumptionInfo;
import com.theitsolutions.meter.reader.service.consumption.domain.response.YearlyConsumptionInfo;
import com.theitsolutions.meter.reader.service.consumption.operator.AggregationOperator;
import com.theitsolutions.meter.reader.service.domain.Address;
import com.theitsolutions.meter.reader.service.domain.ElectricityConsumption;
import com.theitsolutions.meter.reader.service.mapper.AddressMapper;
import com.theitsolutions.meter.reader.service.mapper.ElectricityConsumptionMapper;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.Month;
import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.any;

/**
 * Unit test for {@link ElectricityConsumptionServiceImpl}.
 */
public class ElectricityConsumptionServiceImplTest {

    @Mock
    private ElectricityConsumptionHistoryDao consumptionHistoryDao;

    @Mock
    private AddressDao addressDao;

    @Mock
    private MeterDao meterDao;

    @Mock
    private AddressMapper addressMapper;

    @Mock
    private ElectricityConsumptionMapper consumptionMapper;

    @InjectMocks
    private ElectricityConsumptionServiceImpl underTest;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testReadElectricityConsumptionHistoryShouldReturnAggregatedConsumptionInfo() {

        // given
        given(addressMapper.mapToAddressEntity(any())).willReturn(AddressEntity.builder().build());
        given(meterDao.findByAddress(any())).willReturn(MeterEntity.builder().build());
        given(addressDao.findByClientId(any())).willReturn(List.of(AddressEntity.builder().build()));

        given(consumptionHistoryDao.findByYearAndMeter(any(), any()))
                .willReturn(createTestConsumptionHistories());

        ConsumptionHistoryRequest consumptionHistoryRequest = ConsumptionHistoryRequest.builder()
                .address(Address.builder().build())
                .clientId(1L)
                .month(null)
                .operator(AggregationOperator.SUM)
                .year(2017)
                .build();

        // when
        ElectricityConsumptionInfo consumptionInfo = underTest.readElectricityConsumptionHistory(consumptionHistoryRequest);

        // then
        assertThat(consumptionInfo, instanceOf(AggregatedConsumptionInfo.class));

    }

    @Test
    public void testReadElectricityConsumptionHistoryShouldReturnYearlyConsumptionInfo() {

        // given
        List<ElectricityConsumption> electricityConsumptions = List.of(ElectricityConsumption.builder().build());

        given(addressMapper.mapToAddressEntity(any())).willReturn(AddressEntity.builder().build());
        given(meterDao.findByAddress(any())).willReturn(MeterEntity.builder().build());
        given(addressDao.findByClientId(any())).willReturn(List.of(AddressEntity.builder().build()));
        given(consumptionMapper.mapToElectricityConsumptions(any()))
                .willReturn(electricityConsumptions);

        given(consumptionHistoryDao.findByYearAndMeter(any(), any()))
                .willReturn(createTestConsumptionHistories());

        ConsumptionHistoryRequest consumptionHistoryRequest = ConsumptionHistoryRequest.builder()
                .address(Address.builder().build())
                .clientId(1L)
                .month(null)
                .operator(null)
                .year(2017)
                .build();

        // when
        ElectricityConsumptionInfo consumptionInfo = underTest.readElectricityConsumptionHistory(consumptionHistoryRequest);

        // then
        assertThat(consumptionInfo, instanceOf(YearlyConsumptionInfo.class));

    }

    @Test
    public void testReadElectricityConsumptionHistoryShouldReturnMonthlyConsumptionInfo() {

        // given
        ElectricityConsumption electricityConsumption = ElectricityConsumption.builder()
                .month(Month.AUGUST)
                .year(2017)
                .measuredValue(BigDecimal.ONE)
                .build();

        given(addressMapper.mapToAddressEntity(any())).willReturn(AddressEntity.builder().build());
        given(meterDao.findByAddress(any())).willReturn(MeterEntity.builder().build());
        given(addressDao.findByClientId(any())).willReturn(List.of(AddressEntity.builder().build()));

        given(consumptionMapper.mapToElectricityConsumption(any())).willReturn(electricityConsumption);

        given(consumptionHistoryDao.findByYearAndMonthAndMeter(any(), any(), any()))
                .willReturn(ElectricityConsumptionEntity.builder().build());

        ConsumptionHistoryRequest consumptionHistoryRequest = ConsumptionHistoryRequest.builder()
                .address(Address.builder().build())
                .clientId(1L)
                .month(Month.AUGUST)
                .operator(null)
                .year(2017)
                .build();

        // when
        ElectricityConsumptionInfo consumptionInfo = underTest.readElectricityConsumptionHistory(consumptionHistoryRequest);

        // then
        assertThat(consumptionInfo, instanceOf(MonthlyConsumptionInfo.class));

    }

    private List<ElectricityConsumptionEntity> createTestConsumptionHistories(){
        return List.of(ElectricityConsumptionEntity.builder()
                .measuredValue(BigDecimal.TEN)
                .build());
    }
}
