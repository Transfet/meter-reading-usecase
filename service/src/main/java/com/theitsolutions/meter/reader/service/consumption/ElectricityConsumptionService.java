package com.theitsolutions.meter.reader.service.consumption;

import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.service.consumption.domain.request.ConsumptionHistoryRequest;
import com.theitsolutions.meter.reader.service.consumption.domain.response.ElectricityConsumptionInfo;

/**
 * Service for operating on {@link ElectricityConsumptionEntity}.
 */
public interface ElectricityConsumptionService {

    /**
     * Provides an {@link ElectricityConsumptionInfo} based on the incoming request.
     * @param consumptionHistoryRequest - given {@link ConsumptionHistoryRequest}.
     * @return {@link ElectricityConsumptionInfo} which can be aggregated, yearly or monthly.
     */
    ElectricityConsumptionInfo readElectricityConsumptionHistory(ConsumptionHistoryRequest consumptionHistoryRequest);
}
