package com.theitsolutions.meter.reader.service.consumption.domain.response;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.theitsolutions.meter.reader.service.domain.ElectricityConsumption;

/**
 * Response object for monthly electricity consumption info.
 */
@JsonDeserialize(builder = MonthlyConsumptionInfo.MonthlyConsumptionInfoBuilder.class)
public final class MonthlyConsumptionInfo extends ElectricityConsumptionInfo {

    private ElectricityConsumption electricityConsumption;

    public ElectricityConsumption getElectricityConsumption() {
        return electricityConsumption;
    }

    public void setElectricityConsumption(ElectricityConsumption electricityConsumption) {
        this.electricityConsumption = electricityConsumption;
    }

    public static MonthlyConsumptionInfoBuilder builder() {
        return new MonthlyConsumptionInfoBuilder();
    }

    /**
     * Builder class for {@link MonthlyConsumptionInfo}.
     */
    @JsonPOJOBuilder(withPrefix = "")
    public static final class MonthlyConsumptionInfoBuilder extends ElectricityConsumptionInfoBuilder<MonthlyConsumptionInfo> {

        private ElectricityConsumption electricityConsumption;

        public MonthlyConsumptionInfoBuilder() {
            super(new MonthlyConsumptionInfo());
        }

        public MonthlyConsumptionInfoBuilder electricityConsumption(ElectricityConsumption electricityConsumption) {
            this.electricityConsumption = electricityConsumption;
            return this;
        }

        @Override
        public ElectricityConsumptionInfoBuilder year(int year) {
            return super.year(year);
        }

        @Override
        public MonthlyConsumptionInfo build() {
            MonthlyConsumptionInfo consumptionInfo = super.build();
            consumptionInfo.setElectricityConsumption(electricityConsumption);
            return consumptionInfo;
        }
    }
}
