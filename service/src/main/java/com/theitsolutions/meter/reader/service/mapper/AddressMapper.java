package com.theitsolutions.meter.reader.service.mapper;

import org.springframework.stereotype.Component;

import com.theitsolutions.meter.reader.persistence.entity.AddressEntity;
import com.theitsolutions.meter.reader.service.domain.Address;

/**
 * Class for mapping between {@link Address} and {@link AddressEntity}.
 */
@Component
public class AddressMapper {

    public AddressEntity mapToAddressEntity(Address address) {

        return AddressEntity.builder()
            .houseNumber(address.getHouseNumber())
            .street(address.getStreet())
            .city(address.getCity())
            .country(address.getCountry())
            .build();
    }

}
