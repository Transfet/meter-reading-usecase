package com.theitsolutions.meter.reader.service.consumption.domain.response;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.theitsolutions.meter.reader.service.domain.ElectricityConsumption;

/**
 * Response object for yearly electricity consumption info.
 */
@JsonDeserialize(builder = YearlyConsumptionInfo.ElectricityConsumptionInfoBuilder.class)
public final class YearlyConsumptionInfo extends ElectricityConsumptionInfo {

    private List<ElectricityConsumption> electricityConsumptions;

    public List<ElectricityConsumption> getElectricityConsumptions() {
        return electricityConsumptions;
    }

    public void setElectricityConsumptions(List<ElectricityConsumption> electricityConsumptions) {
        this.electricityConsumptions = electricityConsumptions;
    }

    public static YearlyConsumptionInfoBuilder builder() {
        return new YearlyConsumptionInfoBuilder();
    }

    /**
     * Builder class for {@link YearlyConsumptionInfo}.
     */
    @JsonPOJOBuilder(withPrefix = "")
    public static final class YearlyConsumptionInfoBuilder extends ElectricityConsumptionInfoBuilder<YearlyConsumptionInfo> {

        private List<ElectricityConsumption> electricityConsumptions;

        public YearlyConsumptionInfoBuilder() {
            super(new YearlyConsumptionInfo());
        }

        public YearlyConsumptionInfoBuilder electricityConsumptions(List<ElectricityConsumption> electricityConsumptions) {
            this.electricityConsumptions = electricityConsumptions;
            return this;
        }

        @Override
        public ElectricityConsumptionInfoBuilder year(int year) {
            return super.year(year);
        }

        @Override
        public YearlyConsumptionInfo build() {
            YearlyConsumptionInfo consumptionInfo = super.build();
            consumptionInfo.setElectricityConsumptions(electricityConsumptions);
            return consumptionInfo;
        }
    }
}
