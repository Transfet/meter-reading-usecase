package com.theitsolutions.meter.reader.service.consumption.operator.impl;

import java.math.BigDecimal;
import java.util.List;

import com.theitsolutions.meter.reader.service.consumption.operator.AbstractAggregationOperator;

/**
 * Implementation of {@link AbstractAggregationOperator}.
 */
public class SumConsumptionAggregationOperator extends AbstractAggregationOperator {

    @Override
    protected BigDecimal calculateAggregatedValue(List<BigDecimal> measuredValues) {

        return measuredValues.stream()
            .reduce(BigDecimal::add)
            .orElse(null);
    }
}
