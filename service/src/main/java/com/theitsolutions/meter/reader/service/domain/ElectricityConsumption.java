package com.theitsolutions.meter.reader.service.domain;

import java.math.BigDecimal;
import java.time.Month;
import java.util.Objects;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;

/**
 * A domain class for electricity consumption.
 */
@JsonDeserialize(builder = ElectricityConsumption.ElectricityConsumptionBuilder.class)
public class ElectricityConsumption {

    private int year;

    private Month month;

    private BigDecimal measuredValue;

    public ElectricityConsumption() {
    }

    public ElectricityConsumption(ElectricityConsumptionEntity consumptionHistory) {

        this.measuredValue = consumptionHistory.getMeasuredValue();
        this.year = consumptionHistory.getYear();
        this.month = consumptionHistory.getMonth();
    }

    private ElectricityConsumption(ElectricityConsumptionBuilder builder) {
        this.year = builder.year;
        this.month = builder.month;
        this.measuredValue = builder.measuredValue;
    }

    public static ElectricityConsumptionBuilder builder() {
        return new ElectricityConsumptionBuilder();
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    public BigDecimal getMeasuredValue() {
        return measuredValue;
    }

    public void setMeasuredValue(BigDecimal measuredValue) {
        this.measuredValue = measuredValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ElectricityConsumption that = (ElectricityConsumption) o;
        return year == that.year &&
            month == that.month &&
            Objects.equals(measuredValue, that.measuredValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, month, measuredValue);
    }

    /**
     * Builder class for {@link ElectricityConsumption}.
     */
    @JsonPOJOBuilder(withPrefix = "")
    public static final class ElectricityConsumptionBuilder {
        private int year;
        private Month month;
        private BigDecimal measuredValue;

        private ElectricityConsumptionBuilder() {
        }

        public ElectricityConsumption build() {
            return new ElectricityConsumption(this);
        }

        public ElectricityConsumptionBuilder year(int year) {
            this.year = year;
            return this;
        }

        public ElectricityConsumptionBuilder month(Month month) {
            this.month = month;
            return this;
        }

        public ElectricityConsumptionBuilder measuredValue(BigDecimal measuredValue) {
            this.measuredValue = measuredValue;
            return this;
        }
    }
}
