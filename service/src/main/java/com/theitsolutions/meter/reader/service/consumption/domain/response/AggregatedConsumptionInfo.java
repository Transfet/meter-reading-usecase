package com.theitsolutions.meter.reader.service.consumption.domain.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 * Response object for aggregated electricity consumption info.
 */
@JsonDeserialize(builder = AggregatedConsumptionInfo.AggregatedConsumptionInfoBuilder.class)
public final class AggregatedConsumptionInfo extends ElectricityConsumptionInfo {

    private BigDecimal aggregatedValue;

    private AggregatedConsumptionInfo() {
    }

    public BigDecimal getAggregatedValue() {
        return aggregatedValue;
    }

    public void setAggregatedValue(BigDecimal aggregatedValue) {
        this.aggregatedValue = aggregatedValue;
    }

    public static AggregatedConsumptionInfoBuilder builder() {
        return new AggregatedConsumptionInfoBuilder();
    }

    /**
     * Builder class for {@link AggregatedConsumptionInfo}.
     */
    @JsonPOJOBuilder(withPrefix = "")
    public static final class AggregatedConsumptionInfoBuilder
        extends ElectricityConsumptionInfoBuilder<AggregatedConsumptionInfo> {

        private BigDecimal aggregatedValue;

        public AggregatedConsumptionInfoBuilder() {
            super(new AggregatedConsumptionInfo());
        }

        public AggregatedConsumptionInfoBuilder aggregatedValue(BigDecimal aggregatedValue) {
            this.aggregatedValue = aggregatedValue;
            return this;
        }

        @Override
        public ElectricityConsumptionInfoBuilder year(int year) {
            return super.year(year);
        }

        @Override
        public AggregatedConsumptionInfo build() {
            AggregatedConsumptionInfo consumptionInfo = super.build();
            consumptionInfo.setAggregatedValue(aggregatedValue);
            return consumptionInfo;
        }
    }
}
