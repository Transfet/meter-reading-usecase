package com.theitsolutions.meter.reader.service.meter.domain.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.theitsolutions.meter.reader.service.domain.Address;
import com.theitsolutions.meter.reader.service.domain.ElectricityConsumption;

/**
 * Request object for updating a {@link com.theitsolutions.meter.reader.persistence.entity.MeterEntity}
 * with electricity consumption.
 */
@JsonDeserialize(builder = MeterUpdateRequest.MeterUpdateRequestBuilder.class)
public class MeterUpdateRequest {

    private Long clientId;

    private Address address;

    private ElectricityConsumption electricityConsumption;

    public MeterUpdateRequest() {
    }

    public Long getClientId() {
        return clientId;
    }

    public Address getAddress() {
        return address;
    }

    public ElectricityConsumption getElectricityConsumption() {
        return electricityConsumption;
    }

    public static MeterUpdateRequestBuilder builder() {
        return new MeterUpdateRequestBuilder();
    }

    /**
     * Builder class for {@link MeterUpdateRequest}.
     */
    @JsonPOJOBuilder(withPrefix = "")
    public static final class MeterUpdateRequestBuilder {
        private Long clientId;
        private Address address;
        private ElectricityConsumption electricityConsumption;

        private MeterUpdateRequestBuilder() {
        }

        public MeterUpdateRequestBuilder clientId(Long clientId) {
            this.clientId = clientId;
            return this;
        }

        public MeterUpdateRequestBuilder address(Address address) {
            this.address = address;
            return this;
        }

        public MeterUpdateRequestBuilder electricityConsumption(ElectricityConsumption electricityConsumption) {
            this.electricityConsumption = electricityConsumption;
            return this;
        }

        public MeterUpdateRequest build() {
            MeterUpdateRequest meterUpdateRequest = new MeterUpdateRequest();
            meterUpdateRequest.address = this.address;
            meterUpdateRequest.clientId = this.clientId;
            meterUpdateRequest.electricityConsumption = this.electricityConsumption;
            return meterUpdateRequest;
        }
    }
}
