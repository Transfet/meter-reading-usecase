package com.theitsolutions.meter.reader.service.consumption.operator;

import java.math.BigDecimal;
import java.util.List;

import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.service.consumption.operator.impl.AvgConsumptionAggregationOperator;
import com.theitsolutions.meter.reader.service.consumption.operator.impl.MaxConsumptionAggregationOperator;
import com.theitsolutions.meter.reader.service.consumption.operator.impl.MinConsumptionAggregationOperator;
import com.theitsolutions.meter.reader.service.consumption.operator.impl.SumConsumptionAggregationOperator;

/**
 * Enum class for aggregator operators.
 */
public enum AggregationOperator {

    SUM(new SumConsumptionAggregationOperator()),

    MIN(new MinConsumptionAggregationOperator()),

    MAX(new MaxConsumptionAggregationOperator()),

    AVG(new AvgConsumptionAggregationOperator());

    private ConsumptionAggregationOperator operator;

    AggregationOperator(ConsumptionAggregationOperator operator) {
        this.operator = operator;
    }

    public BigDecimal aggregate(List<ElectricityConsumptionEntity> consumptionHistories) {
        return operator.aggregate(consumptionHistories);
    }
}
