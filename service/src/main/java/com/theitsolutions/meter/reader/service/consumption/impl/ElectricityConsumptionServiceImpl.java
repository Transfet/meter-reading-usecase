package com.theitsolutions.meter.reader.service.consumption.impl;

import static java.util.Objects.nonNull;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theitsolutions.meter.reader.persistence.dao.AddressDao;
import com.theitsolutions.meter.reader.persistence.dao.ElectricityConsumptionHistoryDao;
import com.theitsolutions.meter.reader.persistence.dao.MeterDao;
import com.theitsolutions.meter.reader.persistence.entity.AddressEntity;
import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.persistence.entity.MeterEntity;
import com.theitsolutions.meter.reader.service.consumption.ElectricityConsumptionService;
import com.theitsolutions.meter.reader.service.consumption.domain.request.ConsumptionHistoryRequest;
import com.theitsolutions.meter.reader.service.consumption.domain.response.AggregatedConsumptionInfo;
import com.theitsolutions.meter.reader.service.consumption.domain.response.ElectricityConsumptionInfo;
import com.theitsolutions.meter.reader.service.consumption.domain.response.MonthlyConsumptionInfo;
import com.theitsolutions.meter.reader.service.consumption.domain.response.YearlyConsumptionInfo;
import com.theitsolutions.meter.reader.service.domain.ElectricityConsumption;
import com.theitsolutions.meter.reader.service.mapper.AddressMapper;
import com.theitsolutions.meter.reader.service.mapper.ElectricityConsumptionMapper;

/**
 * Implementation of {@link ElectricityConsumptionService}.
 */
@Service
public class ElectricityConsumptionServiceImpl implements ElectricityConsumptionService {

    private ElectricityConsumptionHistoryDao consumptionHistoryDao;
    private AddressDao addressDao;
    private MeterDao meterDao;
    private AddressMapper addressMapper;
    private ElectricityConsumptionMapper consumptionMapper;

    @Autowired
    public ElectricityConsumptionServiceImpl(ElectricityConsumptionHistoryDao consumptionHistoryDao, AddressDao addressDao,
        MeterDao meterDao, AddressMapper addressMapper,
        ElectricityConsumptionMapper consumptionMapper) {

        this.consumptionHistoryDao = consumptionHistoryDao;
        this.addressDao = addressDao;
        this.meterDao = meterDao;
        this.addressMapper = addressMapper;
        this.consumptionMapper = consumptionMapper;
    }

    @Override
    public ElectricityConsumptionInfo readElectricityConsumptionHistory(ConsumptionHistoryRequest consumptionHistoryRequest) {

        AddressEntity requestedAddress = addressMapper.mapToAddressEntity(consumptionHistoryRequest.getAddress());
        AddressEntity clientAddress = checkAndGetClientAddress(consumptionHistoryRequest.getClientId(), requestedAddress);

        MeterEntity meterEntity = meterDao.findByAddress(clientAddress);

        ElectricityConsumptionInfo electricityConsumptionInfo;

        if (nonNull(consumptionHistoryRequest.getOperator())) {

            electricityConsumptionInfo = getAggregatedConsumptionInfo(consumptionHistoryRequest, meterEntity);
        } else if (nonNull(consumptionHistoryRequest.getMonth())) {

            electricityConsumptionInfo = getMonthlyConsumptionInfo(consumptionHistoryRequest, meterEntity);
        } else {

            electricityConsumptionInfo = getYearlyConsumptionInfo(consumptionHistoryRequest, meterEntity);
        }

        return electricityConsumptionInfo;
    }

    private ElectricityConsumptionInfo getAggregatedConsumptionInfo(ConsumptionHistoryRequest historyRequest, MeterEntity meterEntity) {

        List<ElectricityConsumptionEntity> consumptionHistories =
            consumptionHistoryDao.findByYearAndMeter(historyRequest.getYear(), meterEntity);

        BigDecimal aggregatedValue = historyRequest.getOperator()
            .aggregate(consumptionHistories);

        return AggregatedConsumptionInfo.builder().aggregatedValue(aggregatedValue)
            .year(historyRequest.getYear())
            .build();
    }

    private ElectricityConsumptionInfo getMonthlyConsumptionInfo(ConsumptionHistoryRequest historyRequest, MeterEntity meterEntity) {

        ElectricityConsumptionEntity consumptionHistory = consumptionHistoryDao.findByYearAndMonthAndMeter(historyRequest.getYear(),
            historyRequest.getMonth(), meterEntity);

        ElectricityConsumption singleConsumption = consumptionMapper.mapToElectricityConsumption(consumptionHistory);

        return MonthlyConsumptionInfo.builder()
            .electricityConsumption(singleConsumption)
            .year(historyRequest.getYear())
            .build();
    }

    private ElectricityConsumptionInfo getYearlyConsumptionInfo(ConsumptionHistoryRequest historyRequest, MeterEntity meterEntity) {

        List<ElectricityConsumptionEntity> consumptionHistories =
            consumptionHistoryDao.findByYearAndMeter(historyRequest.getYear(), meterEntity);

        return YearlyConsumptionInfo.builder()
            .electricityConsumptions(consumptionMapper.mapToElectricityConsumptions(consumptionHistories))
            .year(historyRequest.getYear())
            .build();
    }

    private AddressEntity checkAndGetClientAddress(Long clientId, AddressEntity requestedAddress) {

        List<AddressEntity> addresses = addressDao.findByClientId(clientId);

        return addresses.stream()
            .filter(clientAddress -> clientAddress.equals(requestedAddress))
            .findFirst()
            .orElse(null);
    }

}
