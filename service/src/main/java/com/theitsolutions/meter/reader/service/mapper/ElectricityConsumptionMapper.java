package com.theitsolutions.meter.reader.service.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.service.domain.ElectricityConsumption;

/**
 * Class for mapping between {@link ElectricityConsumption} and {@link ElectricityConsumptionEntity}.
 */
@Component
public class ElectricityConsumptionMapper {

    public ElectricityConsumptionEntity mapToElectricityConsumptionHistory(ElectricityConsumption electricityConsumption) {
        return ElectricityConsumptionEntity.builder()
            .month(electricityConsumption.getMonth())
            .year(electricityConsumption.getYear())
            .measuredValue(electricityConsumption.getMeasuredValue())
            .build();
    }

    public ElectricityConsumption mapToElectricityConsumption(ElectricityConsumptionEntity electricityConsumptionEntity) {
        return ElectricityConsumption.builder()
            .month(electricityConsumptionEntity.getMonth())
            .year(electricityConsumptionEntity.getYear())
            .measuredValue(electricityConsumptionEntity.getMeasuredValue())
            .build();
    }

    public List<ElectricityConsumption> mapToElectricityConsumptions(List<ElectricityConsumptionEntity> consumptionHistories) {

        return consumptionHistories.stream()
            .map(ElectricityConsumption::new)
            .collect(Collectors.toList());
    }
}
