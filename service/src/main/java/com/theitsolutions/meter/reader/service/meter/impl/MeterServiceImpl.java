package com.theitsolutions.meter.reader.service.meter.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theitsolutions.meter.reader.persistence.dao.AddressDao;
import com.theitsolutions.meter.reader.persistence.dao.MeterDao;
import com.theitsolutions.meter.reader.persistence.dao.exception.ConsumptionHistoryAlreadyExistsException;
import com.theitsolutions.meter.reader.persistence.entity.AddressEntity;
import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.service.mapper.AddressMapper;
import com.theitsolutions.meter.reader.service.mapper.ElectricityConsumptionMapper;
import com.theitsolutions.meter.reader.service.meter.MeterService;
import com.theitsolutions.meter.reader.service.meter.domain.request.MeterUpdateRequest;

/**
 * Implementation of {@link MeterService}.
 */
@Service
public class MeterServiceImpl implements MeterService {

    private MeterDao meterDao;
    private AddressDao addressDao;
    private AddressMapper addressMapper;
    private ElectricityConsumptionMapper consumptionMapper;

    @Autowired
    public MeterServiceImpl(MeterDao meterDao, AddressDao addressDao,
        AddressMapper addressMapper, ElectricityConsumptionMapper consumptionMapper) {

        this.meterDao = meterDao;
        this.addressDao = addressDao;
        this.addressMapper = addressMapper;
        this.consumptionMapper = consumptionMapper;
    }

    @Override
    public void addConsumptionHistory(MeterUpdateRequest meterUpdateRequest) throws ConsumptionHistoryAlreadyExistsException {

        AddressEntity requestedAddress = addressMapper.mapToAddressEntity(meterUpdateRequest.getAddress());
        List<AddressEntity> addresses = addressDao.findByClientId(meterUpdateRequest.getClientId());

        AddressEntity address = addresses.stream()
            .filter(clientAddress -> clientAddress.equals(requestedAddress))
            .findFirst()
            .orElse(null);

        ElectricityConsumptionEntity consumptionHistory =
            consumptionMapper.mapToElectricityConsumptionHistory(meterUpdateRequest.getElectricityConsumption());

        meterDao.addConsumptionHistoryByAddress(address, consumptionHistory);

    }
}
