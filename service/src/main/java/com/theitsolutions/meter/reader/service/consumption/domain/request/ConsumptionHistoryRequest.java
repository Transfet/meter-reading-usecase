package com.theitsolutions.meter.reader.service.consumption.domain.request;

import java.time.Month;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.theitsolutions.meter.reader.service.consumption.operator.AggregationOperator;
import com.theitsolutions.meter.reader.service.domain.Address;

/**
 * Request class which contains necessary information of getting electricity consumption history.
 */
@JsonDeserialize(builder = ConsumptionHistoryRequest.ConsumptionHistoryRequestBuilder.class)
public final class ConsumptionHistoryRequest {

    @NotNull
    private Long clientId;

    private Address address;

    @NotNull(message = "year must not be null")
    private int year;

    private Month month;

    private AggregationOperator operator;

    private ConsumptionHistoryRequest(ConsumptionHistoryRequestBuilder builder) {
        this.clientId = builder.clientId;
        this.address = builder.address;
        this.year = builder.year;
        this.month = builder.month;
        this.operator = builder.operator;
    }

    public static ConsumptionHistoryRequestBuilder builder() {
        return new ConsumptionHistoryRequestBuilder();
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    public AggregationOperator getOperator() {
        return operator;
    }

    public void setOperator(AggregationOperator operator) {
        this.operator = operator;
    }

    /**
     * Builder class for {@link ConsumptionHistoryRequest}.
     */
    @JsonPOJOBuilder(withPrefix = "")
    public static final class ConsumptionHistoryRequestBuilder {
        private @NotNull Long clientId;
        private @NotNull(message = "Address must not be null") Address address;
        private @NotNull(message = "year must not be null") int year;
        private Month month;
        private AggregationOperator operator;

        private ConsumptionHistoryRequestBuilder() {
        }

        public ConsumptionHistoryRequestBuilder clientId(Long clientId) {
            this.clientId = clientId;
            return this;
        }

        public ConsumptionHistoryRequestBuilder address(Address address) {
            this.address = address;
            return this;
        }

        public ConsumptionHistoryRequestBuilder year(int year) {
            this.year = year;
            return this;
        }

        public ConsumptionHistoryRequestBuilder month(Month month) {
            this.month = month;
            return this;
        }

        public ConsumptionHistoryRequestBuilder operator(AggregationOperator operator) {
            this.operator = operator;
            return this;
        }

        public ConsumptionHistoryRequest build() {
            return new ConsumptionHistoryRequest(this);
        }

    }
}
