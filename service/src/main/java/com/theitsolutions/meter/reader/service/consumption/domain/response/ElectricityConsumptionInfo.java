package com.theitsolutions.meter.reader.service.consumption.domain.response;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.theitsolutions.meter.reader.service.consumption.domain.request.ConsumptionHistoryRequest;

/**
 * Response object for {@link ConsumptionHistoryRequest}.
 */
@JsonDeserialize(builder = ElectricityConsumptionInfo.ElectricityConsumptionInfoBuilder.class)
public abstract class ElectricityConsumptionInfo {

    private int year;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    /**
     * Builder class for {@link ElectricityConsumptionInfo}.
     * @param <T> A child class which extends the {@link ElectricityConsumptionInfo}.
     */
    @JsonPOJOBuilder(withPrefix = "")
    public static class ElectricityConsumptionInfoBuilder<T extends ElectricityConsumptionInfo> {

        private int year;

        private T electricityConsumption;

        protected ElectricityConsumptionInfoBuilder(T electricityConsumption) {
            this.electricityConsumption = electricityConsumption;
        }

        public ElectricityConsumptionInfoBuilder year(int year) {
            this.year = year;
            return this;
        }

        public T build() {
            electricityConsumption.setYear(year);
            return electricityConsumption;
        }
    }
}
