package com.theitsolutions.meter.reader.service.consumption.operator;

import java.math.BigDecimal;
import java.util.List;

import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;

/**
 * Interface for aggregator operators.
 */
public interface ConsumptionAggregationOperator {

    /**
     * Return an aggregated value based on the {@link AggregationOperator}.
     * @param histories - given list of {@link ElectricityConsumptionEntity}.
     * @return - an aggregated value which calculated from the parameter.
     */
    BigDecimal aggregate(List<ElectricityConsumptionEntity> histories);
}
