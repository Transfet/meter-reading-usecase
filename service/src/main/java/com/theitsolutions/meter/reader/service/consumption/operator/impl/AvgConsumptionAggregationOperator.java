package com.theitsolutions.meter.reader.service.consumption.operator.impl;

import static java.math.BigDecimal.valueOf;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import com.theitsolutions.meter.reader.service.consumption.operator.AbstractAggregationOperator;

/**
 * Implementation of {@link AbstractAggregationOperator}.
 */
public class AvgConsumptionAggregationOperator extends AbstractAggregationOperator {

    public static final int AVERAGE_SCALE = 2;

    @Override
    protected BigDecimal calculateAggregatedValue(List<BigDecimal> measuredValues) {
        return measuredValues.stream()
            .reduce(BigDecimal::add)
            .map(value -> value.divide(valueOf(measuredValues.size()), AVERAGE_SCALE, RoundingMode.HALF_UP))
            .orElse(null);
    }
}
