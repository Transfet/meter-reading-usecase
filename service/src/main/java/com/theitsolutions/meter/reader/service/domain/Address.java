package com.theitsolutions.meter.reader.service.domain;

import java.util.Objects;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 * A domain class for address.
 */
@JsonDeserialize(builder = Address.AddressBuilder.class)
public final class Address {

    private String country;
    private String city;
    private String street;
    private Integer houseNumber;

    private Address(AddressBuilder builder) {
        this.country = builder.country;
        this.city = builder.city;
        this.street = builder.street;
        this.houseNumber = builder.houseNumber;
    }

    public static AddressBuilder builder() {
        return new AddressBuilder();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(Integer houseNumber) {
        this.houseNumber = houseNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Address address = (Address) o;
        return Objects.equals(country, address.country) &&
            Objects.equals(city, address.city) &&
            Objects.equals(street, address.street) &&
            Objects.equals(houseNumber, address.houseNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country, city, street, houseNumber);
    }

    /**
     * Builder class for {@link Address}.
     */
    @JsonPOJOBuilder(withPrefix = "")
    public static final class AddressBuilder {
        private String country;
        private String city;
        private String street;
        private Integer houseNumber;

        private AddressBuilder() {
        }

        public Address build() {
            return new Address(this);
        }

        public AddressBuilder country(String country) {
            this.country = country;
            return this;
        }

        public AddressBuilder city(String city) {
            this.city = city;
            return this;
        }

        public AddressBuilder street(String street) {
            this.street = street;
            return this;
        }

        public AddressBuilder houseNumber(Integer houseNumber) {
            this.houseNumber = houseNumber;
            return this;
        }
    }
}
