package com.theitsolutions.meter.reader.service.meter.domain.response;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.theitsolutions.meter.reader.service.domain.ElectricityConsumption;
import com.theitsolutions.meter.reader.service.meter.domain.request.MeterUpdateRequest;

/**
 * Response object for the {@link MeterUpdateRequest}.
 */
@JsonDeserialize(builder = MeterUpdateInfo.MeterUpdateInfoBuilder.class)
public final class MeterUpdateInfo {

    private ElectricityConsumption electricityConsumption;

    private String updateInformation;

    private MeterUpdateInfo(MeterUpdateInfoBuilder builder) {
        this.electricityConsumption = builder.electricityConsumption;
        this.updateInformation = builder.updateInformation;
    }

    public static MeterUpdateInfoBuilder newMeterUpdateInfo() {
        return new MeterUpdateInfoBuilder();
    }

    public ElectricityConsumption getElectricityConsumption() {
        return electricityConsumption;
    }

    public String getUpdateInformation() {
        return updateInformation;
    }

    public static MeterUpdateInfoBuilder builder() {
        return new MeterUpdateInfoBuilder();
    }

    /**
     * Builder class for {@link MeterUpdateInfo}.
     */
    @JsonPOJOBuilder(withPrefix = "")
    public static final class MeterUpdateInfoBuilder {
        private ElectricityConsumption electricityConsumption;
        private String updateInformation;

        private MeterUpdateInfoBuilder() {
        }

        public MeterUpdateInfoBuilder electricityConsumption(ElectricityConsumption electricityConsumption) {
            this.electricityConsumption = electricityConsumption;
            return this;
        }

        public MeterUpdateInfoBuilder updateInformation(String updateInformation) {
            this.updateInformation = updateInformation;
            return this;
        }

        public MeterUpdateInfo build() {
            return new MeterUpdateInfo(this);
        }

    }
}
