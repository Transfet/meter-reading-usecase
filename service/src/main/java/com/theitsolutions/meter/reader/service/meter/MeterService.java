package com.theitsolutions.meter.reader.service.meter;

import com.theitsolutions.meter.reader.persistence.dao.exception.ConsumptionHistoryAlreadyExistsException;
import com.theitsolutions.meter.reader.service.meter.domain.request.MeterUpdateRequest;

/**
 * Service for operating on {@link com.theitsolutions.meter.reader.persistence.entity.MeterEntity}.
 */
public interface MeterService {

    /**
     * Add an electricity consumption to an existing meter.
     * @param meterUpdateRequest - given {@link MeterUpdateRequest}.
     * @throws ConsumptionHistoryAlreadyExistsException - it occurs when the given electricity consumption has already exists
     * in the given month.
     */
    void addConsumptionHistory(MeterUpdateRequest meterUpdateRequest) throws ConsumptionHistoryAlreadyExistsException;
}
