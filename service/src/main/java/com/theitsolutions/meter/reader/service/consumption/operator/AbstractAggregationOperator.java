package com.theitsolutions.meter.reader.service.consumption.operator;

import static org.springframework.util.Assert.notNull;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;

/**
 * Implementation of {@link ConsumptionAggregationOperator}.
 */
public abstract class AbstractAggregationOperator implements ConsumptionAggregationOperator {

    @Override
    public BigDecimal aggregate(List<ElectricityConsumptionEntity> histories) {

        notNull(histories, "Argument must not be null");

        List<BigDecimal> consumptionValues = histories.stream()
            .map(ElectricityConsumptionEntity::getMeasuredValue)
            .collect(Collectors.toList());

        return calculateAggregatedValue(consumptionValues);
    }

    protected abstract BigDecimal calculateAggregatedValue(List<BigDecimal> measuredValues);
}
