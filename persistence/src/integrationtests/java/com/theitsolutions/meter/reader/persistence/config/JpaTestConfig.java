package com.theitsolutions.meter.reader.persistence.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Configuration class for integration tests.
 */
@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = {"com.theitsolutions.meter.reader.persistence.repository"})
@EntityScan(basePackages = {"com.theitsolutions.meter.reader.persistence.entity"})
@ComponentScan(basePackages = {"com.theitsolutions.meter.reader.persistence"})
public class JpaTestConfig {
}
