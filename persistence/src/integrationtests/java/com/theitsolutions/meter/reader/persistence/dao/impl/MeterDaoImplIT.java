package com.theitsolutions.meter.reader.persistence.dao.impl;

import static java.time.Month.APRIL;
import static java.time.Month.AUGUST;
import static java.time.Month.OCTOBER;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.theitsolutions.meter.reader.persistence.config.JpaTestConfig;
import com.theitsolutions.meter.reader.persistence.dao.MeterDao;
import com.theitsolutions.meter.reader.persistence.dao.exception.ConsumptionHistoryAlreadyExistsException;
import com.theitsolutions.meter.reader.persistence.entity.AddressEntity;
import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.persistence.entity.MeterEntity;
import com.theitsolutions.meter.reader.persistence.repository.AddressRepository;
import com.theitsolutions.meter.reader.persistence.repository.MeterRepository;

/**
 * Integration test for {@link MeterDaoImpl}.
 */
@ContextConfiguration(classes = JpaTestConfig.class)
@DataJpaTest
@ActiveProfiles("test")
public class MeterDaoImplIT extends AbstractTestNGSpringContextTests {

    private static final int TEST_CONSUMPTION_YEAR = 2017;

    @Autowired
    private MeterRepository meterRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private MeterDao underTest;

    @BeforeMethod
    public void setUp() {

        addressRepository.saveAndFlush(createTestAddress());
        meterRepository.save(createTestMeter());
    }

    @DirtiesContext
    @Test
    public void testAddConsumptionHistoryShouldAddConsumptionIfConsumptionNotExists() {

        // given
        Optional<AddressEntity> address = addressRepository.findById(1L);

        ElectricityConsumptionEntity consumptionHistory = ElectricityConsumptionEntity.builder()
            .month(APRIL)
            .year(TEST_CONSUMPTION_YEAR)
            .build();

        // when
        underTest.addConsumptionHistoryByAddress(address.get(), consumptionHistory);

        // then
        MeterEntity meterEntity = meterRepository.findByAddress(address.get());
        assertThat(meterEntity.getConsumptionHistories(), hasItem(consumptionHistory));

    }

    @DirtiesContext
    @Test
    public void testFindByAddressShouldReturnMeter() {

        // given
        Optional<AddressEntity> address = addressRepository.findById(1L);
        Optional<MeterEntity> expectedMeter = meterRepository.findById(1L);

        // when
        MeterEntity meterEntity = underTest.findByAddress(address.get());

        // then
        assertThat(meterEntity, is(expectedMeter.get()));

    }


    @DirtiesContext
    @Test(expectedExceptions = ConsumptionHistoryAlreadyExistsException.class)
    public void testAddConsumptionHistoryShouldThrowExceptionIfConsumptionIsExists() throws ConsumptionHistoryAlreadyExistsException {

        // given
        Optional<AddressEntity> address = addressRepository.findById(1L);

        ElectricityConsumptionEntity consumptionHistory = ElectricityConsumptionEntity.builder()
            .month(AUGUST)
            .year(TEST_CONSUMPTION_YEAR)
            .build();

        // when
        underTest.addConsumptionHistoryByAddress(address.get(), consumptionHistory);

        // then throw exception

    }

    private AddressEntity createTestAddress() {
        return AddressEntity.builder()
            .city("Debrecen")
            .country("Hungary")
            .street("Piac utca")
            .houseNumber(21)
            .build();
    }

    private List<ElectricityConsumptionEntity> createTestConsumptions() {

        return List.of(
            ElectricityConsumptionEntity.builder()
                .year(TEST_CONSUMPTION_YEAR)
                .month(AUGUST)
                .build(),

            ElectricityConsumptionEntity.builder()
                .year(TEST_CONSUMPTION_YEAR)
                .month(OCTOBER)
                .build()
        );
    }

    private MeterEntity createTestMeter() {

        Optional<AddressEntity> address = addressRepository.findById(1L);

        return MeterEntity.builder()
            .address(address.get())
            .consumptionHistories(createTestConsumptions())
            .build();
    }
}
