package com.theitsolutions.meter.reader.persistence.repository;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

import java.time.Month;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.theitsolutions.meter.reader.persistence.config.JpaTestConfig;
import com.theitsolutions.meter.reader.persistence.entity.AddressEntity;
import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.persistence.entity.MeterEntity;

/**
 * Integration tests for {@link ElectricityConsumptionHistoryRepository}.
 */
@ContextConfiguration(classes = {JpaTestConfig.class})
@DataJpaTest
public class ElectricityConsumptionHistoryRepositoryIT extends AbstractTestNGSpringContextTests {

    private static final int TEST_CONSUMPTION_YEAR = 2017;
    private static final Month TEST_CONSUMPTION_MONTH = Month.MARCH;

    @Autowired
    private ElectricityConsumptionHistoryRepository underTest;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private MeterRepository meterRepository;

    @BeforeMethod
    public void setUp() {

        // global given
        addressRepository.saveAndFlush(createTestAddress());
        meterRepository.save(createTestMeter());
    }

    @DirtiesContext
    @Test
    public void testFindByYearAndAddressShouldReturnConsumptionsIfYearAndAddressAreMatched() {

        // given
        Optional<AddressEntity> address = addressRepository.findById(1L);
        MeterEntity meterEntity = meterRepository.findByAddress(address.get());

        // when
        List<ElectricityConsumptionEntity> histories = underTest.findByYearAndMeter(2017, meterEntity);

        // then
        assertThat(histories, hasSize(2));

    }

    @DirtiesContext
    @Test
    public void testFindByYearMonthAndAddressShouldReturnConsumptionIfYearMonthAddressAreMatched() {

        // given
        Optional<AddressEntity> address = addressRepository.findById(1L);
        MeterEntity meterEntity = meterRepository.findByAddress(address.get());

        // when
        ElectricityConsumptionEntity history =
            underTest.findByYearMonthAndMeter(TEST_CONSUMPTION_YEAR, TEST_CONSUMPTION_MONTH, meterEntity);

        // then
        assertThat(history.getMonth(), is(TEST_CONSUMPTION_MONTH));
        assertThat(history.getYear(), is(TEST_CONSUMPTION_YEAR));

    }

    private List<ElectricityConsumptionEntity> createTestConsumptions() {

        return List.of(
            ElectricityConsumptionEntity.builder()
                .year(TEST_CONSUMPTION_YEAR)
                .month(TEST_CONSUMPTION_MONTH)
                .build(),

            ElectricityConsumptionEntity.builder()
                .year(TEST_CONSUMPTION_YEAR)
                .build()
        );
    }

    private AddressEntity createTestAddress() {
        return AddressEntity.builder()
            .city("Debrecen")
            .country("Hungary")
            .houseNumber(21)
            .street("Piac utca")
            .build();
    }

    private MeterEntity createTestMeter() {

        Optional<AddressEntity> address = addressRepository.findById(1L);

        return MeterEntity.builder()
            .address(address.get())
            .consumptionHistories(createTestConsumptions())
            .build();
    }
}
