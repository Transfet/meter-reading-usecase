CREATE TABLE CLIENT(
 ID bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
);

create table ADDRESS(
 ID bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
  COUNTRY varchar(30),
  CITY varchar(30),
  STREET varchar(30),
  HOUSE_NUMBER INTEGER
);

create table client_address_sw(
client_fk bigint not null,
address_fk bigint not null,
constraint UK_address_fk unique (address_fk),
constraint FK_address_fk foreign key (address_fk) references ADDRESS(ID),
constraint FK_client_fk foreign key (client_fk) references CLIENT(ID)
);

create table METER(
    ID bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    ADDRESS_ID bigint not null,
    constraint FK_meter_client foreign key (ADDRESS_ID) references ADDRESS(ID)
);

create table ELECTRICITY_CONSUMPTION_HISTORY(
    ID integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
    MEASURED_VALUE decimal,
    YEAR smallint,
    MONTH smallint,
    METER_ID bigint,
    constraint FK_meter_consumption foreign key (METER_ID) references METER(ID)
);

