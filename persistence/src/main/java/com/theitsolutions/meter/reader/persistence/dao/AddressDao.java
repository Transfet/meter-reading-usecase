package com.theitsolutions.meter.reader.persistence.dao;

import java.util.List;

import com.theitsolutions.meter.reader.persistence.entity.AddressEntity;

/**
 * Dao interface for {@link AddressEntity}.
 */
public interface AddressDao {

    /**
     * Provide list of {@link AddressEntity} by client id.
     * @param clientId - given clientId.
     * @return - list of address entity by the given clientId.
     */
    List<AddressEntity> findByClientId(Long clientId);
}
