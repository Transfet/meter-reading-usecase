package com.theitsolutions.meter.reader.persistence.entity;

import java.math.BigDecimal;
import java.time.Month;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class which holds information about the measured electricity consumption.
 */
@Entity
@Table(name = "ELECTRICITY_CONSUMPTION_HISTORY")
public class ElectricityConsumptionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "MEASURED_VALUE")
    private BigDecimal measuredValue;

    @Column(name = "YEAR")
    private Integer year;

    @Column(name = "MONTH", columnDefinition = "smallint")
    @Enumerated
    private Month month;

    public ElectricityConsumptionEntity() {
    }

    private ElectricityConsumptionEntity(ElectricityConsumptionEntityBuilder builder) {
        this.id = builder.id;
        this.measuredValue = builder.measuredValue;
        this.month = builder.month;
        this.year = builder.year;
    }

    public static ElectricityConsumptionEntityBuilder builder() {
        return new ElectricityConsumptionEntityBuilder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getMeasuredValue() {
        return measuredValue;
    }

    public void setMeasuredValue(BigDecimal measuredValue) {
        this.measuredValue = measuredValue;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }

        if (!(o instanceof ElectricityConsumptionEntity)) {
            return false;
        }

        ElectricityConsumptionEntity that = (ElectricityConsumptionEntity) o;
        return Objects.equals(year, that.year) &&
            month == that.month;
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, month);
    }

    /**
     * Builder class for {@link ElectricityConsumptionEntity}.
     */
    public static final class ElectricityConsumptionEntityBuilder {
        private Long id;
        private BigDecimal measuredValue;
        private Integer year;
        private Month month;

        private ElectricityConsumptionEntityBuilder() {
        }

        public ElectricityConsumptionEntityBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public ElectricityConsumptionEntityBuilder measuredValue(BigDecimal measuredValue) {
            this.measuredValue = measuredValue;
            return this;
        }

        public ElectricityConsumptionEntityBuilder year(Integer year) {
            this.year = year;
            return this;
        }

        public ElectricityConsumptionEntityBuilder month(Month month) {
            this.month = month;
            return this;
        }

        public ElectricityConsumptionEntity build() {
            return new ElectricityConsumptionEntity(this);
        }
    }
}
