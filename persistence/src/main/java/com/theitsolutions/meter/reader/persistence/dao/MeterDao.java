package com.theitsolutions.meter.reader.persistence.dao;

import com.theitsolutions.meter.reader.persistence.dao.exception.ConsumptionHistoryAlreadyExistsException;
import com.theitsolutions.meter.reader.persistence.entity.AddressEntity;
import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.persistence.entity.MeterEntity;

/**
 * Interface for {@link MeterEntity}.
 */
public interface MeterDao {

    /**
     * Add an electricity consumption history to an existing meter.
     * @param address - given {@link AddressEntity} where the meter is installed.
     * @param consumptionHistory - given {@link ElectricityConsumptionEntity}.
     * @throws ConsumptionHistoryAlreadyExistsException - it occurs when the consumption has already exists
     * in the given month.
     */
    void addConsumptionHistoryByAddress(AddressEntity address, ElectricityConsumptionEntity consumptionHistory)
        throws ConsumptionHistoryAlreadyExistsException;

    /**
     * Provides a meter by address.
     * @param address - given {@link AddressEntity}.
     * @return {@link MeterEntity} of the given address.
     */
    MeterEntity findByAddress(AddressEntity address);
}
