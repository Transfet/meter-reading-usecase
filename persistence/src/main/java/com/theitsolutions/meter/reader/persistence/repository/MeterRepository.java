package com.theitsolutions.meter.reader.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.theitsolutions.meter.reader.persistence.entity.AddressEntity;
import com.theitsolutions.meter.reader.persistence.entity.MeterEntity;

/**
 * Repository for {@link MeterEntity}.
 */
@Repository
public interface MeterRepository extends JpaRepository<MeterEntity, Long> {

    /**
     * Provide a {@link MeterEntity} by address.
     * @param address - given {@link AddressEntity} where the meter has been installed.
     * @return {@link MeterEntity} which is installed at the {@link AddressEntity} parameter.
     */
    MeterEntity findByAddress(AddressEntity address);
}
