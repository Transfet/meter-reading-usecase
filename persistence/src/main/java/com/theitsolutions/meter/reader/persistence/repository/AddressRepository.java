package com.theitsolutions.meter.reader.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.theitsolutions.meter.reader.persistence.entity.AddressEntity;

/**
 * Repository for {@link AddressEntity}.
 */
@Repository
public interface AddressRepository extends JpaRepository<AddressEntity, Long> {

    /**
     * Provides a client addresses.
     * @param clientId - given client id.
     * @return - a list of {@link AddressEntity}.
     */
    @Query("select c.addresses from ClientEntity c where c.id = ?1")
    List<AddressEntity> findByClientId(Long clientId);
}
