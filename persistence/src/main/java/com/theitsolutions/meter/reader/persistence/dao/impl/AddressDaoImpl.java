package com.theitsolutions.meter.reader.persistence.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.theitsolutions.meter.reader.persistence.dao.AddressDao;
import com.theitsolutions.meter.reader.persistence.entity.AddressEntity;
import com.theitsolutions.meter.reader.persistence.repository.AddressRepository;

/**
 * Implementation of {@link AddressDao}.
 */
@Component
public class AddressDaoImpl implements AddressDao {

    private AddressRepository addressRepository;

    @Autowired
    public AddressDaoImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public List<AddressEntity> findByClientId(Long clientId) {
        return addressRepository.findByClientId(clientId);
    }
}
