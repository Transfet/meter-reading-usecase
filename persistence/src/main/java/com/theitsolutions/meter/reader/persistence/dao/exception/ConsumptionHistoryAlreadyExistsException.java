package com.theitsolutions.meter.reader.persistence.dao.exception;

/**
 * Exception which is occur when an electricity consumption has already exists.
 */
public class ConsumptionHistoryAlreadyExistsException extends RuntimeException {

    public ConsumptionHistoryAlreadyExistsException() {
    }
}
