package com.theitsolutions.meter.reader.persistence.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity class which holds all information about the {@link ClientEntity} address.
 */
@Entity
@Table(name = "ADDRESS")
public class AddressEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "COUNTRY")
    private String country;

    @Column(name = "CITY")
    private String city;

    @Column(name = "STREET")
    private String street;

    @Column(name = "HOUSE_NUMBER")
    private Integer houseNumber;

    @OneToOne(mappedBy = "address", targetEntity = MeterEntity.class)
    private MeterEntity meterEntity;

    public AddressEntity() {
    }

    private AddressEntity(AddressEntityBuilder builder) {
        this.id = builder.id;
        this.country = builder.country;
        this.city = builder.city;
        this.street = builder.street;
        this.houseNumber = builder.houseNumber;
        this.meterEntity = builder.meterEntity;
    }

    public static AddressEntityBuilder builder() {
        return new AddressEntityBuilder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(Integer houseNumber) {
        this.houseNumber = houseNumber;
    }

    public MeterEntity getMeterEntity() {
        return meterEntity;
    }

    public void setMeterEntity(MeterEntity meterEntity) {
        this.meterEntity = meterEntity;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AddressEntity that = (AddressEntity) o;
        return Objects.equals(country, that.country) &&
            Objects.equals(city, that.city) &&
            Objects.equals(street, that.street) &&
            Objects.equals(houseNumber, that.houseNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country, city, street, houseNumber);
    }

    /**
     * Builder class for {@link AddressEntity}.
     */
    public static final class AddressEntityBuilder {
        private Long id;
        private String country;
        private String city;
        private String street;
        private Integer houseNumber;
        private MeterEntity meterEntity;

        private AddressEntityBuilder() {
        }

        public AddressEntityBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public AddressEntityBuilder country(String country) {
            this.country = country;
            return this;
        }

        public AddressEntityBuilder city(String city) {
            this.city = city;
            return this;
        }

        public AddressEntityBuilder street(String street) {
            this.street = street;
            return this;
        }

        public AddressEntityBuilder houseNumber(Integer houseNumber) {
            this.houseNumber = houseNumber;
            return this;
        }

        public AddressEntityBuilder meter(MeterEntity meterEntity) {
            this.meterEntity = meterEntity;
            return this;
        }

        public AddressEntity build() {
            return new AddressEntity(this);
        }
    }
}
