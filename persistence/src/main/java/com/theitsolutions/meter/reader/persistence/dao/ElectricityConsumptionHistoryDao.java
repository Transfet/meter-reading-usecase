package com.theitsolutions.meter.reader.persistence.dao;

import java.time.Month;
import java.util.List;

import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.persistence.entity.MeterEntity;

/**
 * Interface for {@link ElectricityConsumptionEntity}.
 */
public interface ElectricityConsumptionHistoryDao {

    /**
     * This method provides all {@link ElectricityConsumptionEntity} grouped by the given year.
     * @param year - given year as int.
     * @param meterEntity - given {@link MeterEntity}.
     * @return - A list of electricity consumption.
     */
    List<ElectricityConsumptionEntity> findByYearAndMeter(Integer year, MeterEntity meterEntity);

    /**
     * This method provides a specific {@link ElectricityConsumptionEntity}.
     * if the year and the month matched with the parameters.
     * @param year - given year as int.
     * @param month - given {@link Month}.
     * @param meterEntity - given {@link MeterEntity}.
     * @return - A single electricity consumption history.
     */
    ElectricityConsumptionEntity findByYearAndMonthAndMeter(Integer year, Month month, MeterEntity meterEntity);

}
