package com.theitsolutions.meter.reader.persistence.entity;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity class which holds information about the ClientEntity.
 */
@Entity
@Table(name = "CLIENT")
public class ClientEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "client_address_sw",
        joinColumns = {@JoinColumn(name = "CLIENT_FK", referencedColumnName = "ID")},
        inverseJoinColumns = {@JoinColumn(name = "ADDRESS_FK", referencedColumnName = "ID")})
    private List<AddressEntity> addresses;

    public ClientEntity() {
    }

    private ClientEntity(ClientEntityBuilder builder) {
        this.id = builder.id;
        this.addresses = builder.addresses;
    }

    public static ClientEntityBuilder builder() {
        return new ClientEntityBuilder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<AddressEntity> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<AddressEntity> addresses) {
        this.addresses = addresses;
    }

    /**
     * Builder class for {@link ClientEntity}.
     */
    public static final class ClientEntityBuilder {
        private Long id;
        private List<AddressEntity> addresses;

        private ClientEntityBuilder() {
        }

        public ClientEntityBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public ClientEntityBuilder address(List<AddressEntity> addresses) {
            this.addresses = addresses;
            return this;
        }

        public ClientEntity build() {
            return new ClientEntity(this);
        }
    }
}
