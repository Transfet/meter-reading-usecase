package com.theitsolutions.meter.reader.persistence.dao.impl;

import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.theitsolutions.meter.reader.persistence.dao.MeterDao;
import com.theitsolutions.meter.reader.persistence.dao.exception.ConsumptionHistoryAlreadyExistsException;
import com.theitsolutions.meter.reader.persistence.entity.AddressEntity;
import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.persistence.entity.MeterEntity;
import com.theitsolutions.meter.reader.persistence.repository.MeterRepository;

/**
 * Implementation of {@link MeterDao}.
 */
@Component
public class MeterDaoImpl implements MeterDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(MeterDaoImpl.class);
    private MeterRepository meterRepository;

    @Autowired
    public MeterDaoImpl(MeterRepository meterRepository) {
        this.meterRepository = meterRepository;
    }

    @Transactional
    @Override
    public void addConsumptionHistoryByAddress(AddressEntity address, ElectricityConsumptionEntity consumptionHistory) {

        MeterEntity meterEntity = meterRepository.findByAddress(address);

        if (isConsumptionHistoryExists(meterEntity, consumptionHistory)) {
            LOGGER.error("Consumption history already exists with year={} and month={}", consumptionHistory.getYear(),
                consumptionHistory.getMonth());

            throw new ConsumptionHistoryAlreadyExistsException();
        }

        meterEntity.getConsumptionHistories()
            .add(consumptionHistory);

    }

    @Override
    public MeterEntity findByAddress(AddressEntity address) {
        return meterRepository.findByAddress(address);
    }

    private boolean isConsumptionHistoryExists(MeterEntity meterEntity, ElectricityConsumptionEntity consumptionHistory) {

        Predicate<ElectricityConsumptionEntity> newConsumptionHistory =
            electricityConsumptionHistory -> electricityConsumptionHistory.equals(consumptionHistory);

        return meterEntity.getConsumptionHistories()
            .stream()
            .anyMatch(newConsumptionHistory);
    }
}
