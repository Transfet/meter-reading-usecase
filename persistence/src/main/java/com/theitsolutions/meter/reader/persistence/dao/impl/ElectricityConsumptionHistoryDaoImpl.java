package com.theitsolutions.meter.reader.persistence.dao.impl;


import java.time.Month;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.theitsolutions.meter.reader.persistence.dao.ElectricityConsumptionHistoryDao;
import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.persistence.entity.MeterEntity;
import com.theitsolutions.meter.reader.persistence.repository.ElectricityConsumptionHistoryRepository;

/**
 * Implementation of {@link ElectricityConsumptionHistoryDao}.
 */
@Component
public class ElectricityConsumptionHistoryDaoImpl implements ElectricityConsumptionHistoryDao {

    private ElectricityConsumptionHistoryRepository electricityConsumptionHistoryRepository;

    @Autowired
    public ElectricityConsumptionHistoryDaoImpl(ElectricityConsumptionHistoryRepository electricityConsumptionHistoryRepository) {
        this.electricityConsumptionHistoryRepository = electricityConsumptionHistoryRepository;
    }

    @Override
    public List<ElectricityConsumptionEntity> findByYearAndMeter(Integer year, MeterEntity meterEntity) {

        return electricityConsumptionHistoryRepository.findByYearAndMeter(year, meterEntity);
    }

    @Override
    public ElectricityConsumptionEntity findByYearAndMonthAndMeter(Integer year, Month month, MeterEntity meterEntity) {
        return electricityConsumptionHistoryRepository.findByYearMonthAndMeter(year, month, meterEntity);
    }
}
