package com.theitsolutions.meter.reader.persistence.entity;

import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity class which holds information about the smart meter,
 * including the historical data about the energy consumption.
 */
@Entity
@Table(name = "METER")
public class MeterEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "METER_ID")
    private List<ElectricityConsumptionEntity> consumptionHistories;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "address_id")
    private AddressEntity address;

    public MeterEntity() {
    }

    private MeterEntity(MeterBuilder builder) {
        this.id = builder.id;
        this.consumptionHistories = builder.consumptionHistories;
        this.address = builder.address;
    }

    public static MeterBuilder builder() {
        return new MeterBuilder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<ElectricityConsumptionEntity> getConsumptionHistories() {
        return consumptionHistories;
    }

    public void setConsumptionHistories(List<ElectricityConsumptionEntity> consumptionHistories) {
        this.consumptionHistories = consumptionHistories;
    }

    public AddressEntity getAddress() {
        return address;
    }

    public void setAddress(AddressEntity address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }

        if (!(o instanceof MeterEntity)) {
            return false;
        }

        MeterEntity that = (MeterEntity) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(address, that.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, address);
    }

    /**
     * Builder class for {@link MeterEntity}.
     */
    public static final class MeterBuilder {

        private Long id;
        private List<ElectricityConsumptionEntity> consumptionHistories;
        private AddressEntity address;

        private MeterBuilder() {
        }

        public MeterBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public MeterBuilder consumptionHistories(List<ElectricityConsumptionEntity> consumptionHistories) {
            this.consumptionHistories = consumptionHistories;
            return this;
        }

        public MeterBuilder address(AddressEntity address) {
            this.address = address;
            return this;
        }

        public MeterEntity build() {
            return new MeterEntity(this);
        }
    }
}
