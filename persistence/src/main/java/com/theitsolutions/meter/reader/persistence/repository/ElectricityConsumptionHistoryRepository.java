package com.theitsolutions.meter.reader.persistence.repository;

import java.time.Month;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.persistence.entity.MeterEntity;

/**
 * Repository for {@link ElectricityConsumptionEntity}.
 */
@Repository
public interface ElectricityConsumptionHistoryRepository extends JpaRepository<ElectricityConsumptionEntity, Long> {

    /**
     * This method provides all {@link ElectricityConsumptionEntity} grouped by the given year.
     * @param year - given year as int.
     * @param meterEntity - given {@link MeterEntity}.
     * @return - A list of electricity consumption.
     */
    @Query("select ech from MeterEntity m inner join m.consumptionHistories ech where m = ?2 and ech.year = ?1")
    List<ElectricityConsumptionEntity> findByYearAndMeter(Integer year, MeterEntity meterEntity);

    /**
     * This method provides a specific {@link ElectricityConsumptionEntity}.
     * if the year and the month matched with the parameters.
     * @param year - given year as int.
     * @param month - given {@link Month}.
     * @param meterEntity - given {@link MeterEntity}.
     * @return - A single electricity consumption history.
     */
    @Query("select ech from MeterEntity m inner join m.consumptionHistories ech where m = ?3 " +
        "and ech.year = ?1 and ech.month = ?2")
    ElectricityConsumptionEntity findByYearMonthAndMeter(Integer year, Month month, MeterEntity meterEntity);
}
