package com.theitsolutions.meter.reader.persistence.dao.impl;

import static java.time.Month.AUGUST;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.theitsolutions.meter.reader.persistence.dao.exception.ConsumptionHistoryAlreadyExistsException;
import com.theitsolutions.meter.reader.persistence.entity.AddressEntity;
import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.persistence.entity.MeterEntity;
import com.theitsolutions.meter.reader.persistence.repository.MeterRepository;


/**
 * Unit test for {@link MeterDaoImpl}.
 */
public class MeterDaoImplTest {

    private static final AddressEntity TEST_ADDRESS = AddressEntity.builder().build();

    @Mock
    private MeterRepository meterRepository;

    @InjectMocks
    private MeterDaoImpl underTest;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddConsumptionHistoryShouldAddConsumptionIfConsumptionNotExists() {

        // given
        MeterEntity meterEntity = MeterEntity.builder()
            .address(TEST_ADDRESS)
            .consumptionHistories(new ArrayList<>())
            .build();

        ElectricityConsumptionEntity consumptionHistory = ElectricityConsumptionEntity.builder()
            .year(2019)
            .month(AUGUST)
            .build();

        given(meterRepository.findByAddress(any(AddressEntity.class))).willReturn(meterEntity);

        // when
        underTest.addConsumptionHistoryByAddress(TEST_ADDRESS, consumptionHistory);

        // then
        verify(meterRepository).findByAddress(TEST_ADDRESS);
    }

    @Test
    public void testFindByAddressShouldReturnMeter() {

        // given
        MeterEntity expectedMeterEntity = MeterEntity.builder().build();
        AddressEntity address = AddressEntity.builder().build();
        given(meterRepository.findByAddress(any(AddressEntity.class))).willReturn(expectedMeterEntity);

        // when
        MeterEntity meterEntity = underTest.findByAddress(address);

        // then
        assertThat(meterEntity, is(expectedMeterEntity));

    }

    @Test(expectedExceptions = ConsumptionHistoryAlreadyExistsException.class)
    public void testAddConsumptionShouldThrowExceptionIfConsumptionIsExists() throws ConsumptionHistoryAlreadyExistsException {

        // given
        ElectricityConsumptionEntity consumptionHistory = ElectricityConsumptionEntity.builder()
            .year(2019)
            .month(AUGUST)
            .build();

        MeterEntity meterEntity = MeterEntity.builder()
            .address(TEST_ADDRESS)
            .consumptionHistories(Arrays.asList(consumptionHistory))
            .build();

        given(meterRepository.findByAddress(any(AddressEntity.class))).willReturn(meterEntity);

        // when
        underTest.addConsumptionHistoryByAddress(TEST_ADDRESS, consumptionHistory);

        // then throw exception
    }
}
