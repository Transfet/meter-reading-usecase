package com.theitsolutions.meter.reader.persistence.dao.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;

import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.theitsolutions.meter.reader.persistence.entity.AddressEntity;
import com.theitsolutions.meter.reader.persistence.repository.AddressRepository;

/**
 * Unit test for {@link AddressDaoImpl}.
 */
public class AddressEntityDaoImplTest {

    @Mock
    private AddressRepository addressRepository;

    @InjectMocks
    private AddressDaoImpl underTest;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findByClientIdShouldReturnAddress() {

        // given
        List<AddressEntity> expectedAddress = List.of(AddressEntity.builder()
            .build());

        given(addressRepository.findByClientId(anyLong())).willReturn(expectedAddress);

        // when
        List<AddressEntity> addresses = underTest.findByClientId(1L);

        // then
        assertThat(addresses, is(expectedAddress));

    }
}
