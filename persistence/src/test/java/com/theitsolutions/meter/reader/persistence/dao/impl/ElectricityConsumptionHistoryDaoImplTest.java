package com.theitsolutions.meter.reader.persistence.dao.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import java.time.Month;
import java.util.Collections;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.theitsolutions.meter.reader.persistence.entity.ElectricityConsumptionEntity;
import com.theitsolutions.meter.reader.persistence.entity.MeterEntity;
import com.theitsolutions.meter.reader.persistence.repository.ElectricityConsumptionHistoryRepository;

/**
 * Unit test for {@link ElectricityConsumptionHistoryDaoImpl}.
 */
public class ElectricityConsumptionHistoryDaoImplTest {

    private static final int TEST_YEAR = 2019;
    private static final Month TEST_MONTH = Month.AUGUST;
    private static final MeterEntity TEST_ADDRESS = MeterEntity.builder().build();

    @Mock
    private ElectricityConsumptionHistoryRepository electricityConsumptionHistoryRepository;

    @InjectMocks
    private ElectricityConsumptionHistoryDaoImpl underTest;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindByYearShouldReturnConsumptionsIfYearMatched() {

        // given
        List<ElectricityConsumptionEntity> expectedConsumptions =
            Collections.singletonList(ElectricityConsumptionEntity.builder().build());

        given(electricityConsumptionHistoryRepository.findByYearAndMeter(any(), any()))
            .willReturn(expectedConsumptions);

        // when
        List<ElectricityConsumptionEntity> consumptions = underTest.findByYearAndMeter(TEST_YEAR, MeterEntity.builder().build());

        // then
        assertThat(consumptions, is(expectedConsumptions));

    }

    @Test
    public void testFindByYearAndMonthShouldReturnConsumptionIfYearAndMonthMatched() {

        // given
        ElectricityConsumptionEntity expectedConsumption = ElectricityConsumptionEntity.builder().build();

        given(electricityConsumptionHistoryRepository.findByYearMonthAndMeter(any(), any(), any()))
            .willReturn(expectedConsumption);

        // when
        ElectricityConsumptionEntity consumption = underTest.findByYearAndMonthAndMeter(TEST_YEAR, TEST_MONTH, TEST_ADDRESS);

        // then
        assertThat(consumption, is(expectedConsumption));

    }
}
